﻿using UnityEngine;
using System.Collections;

public class InputSettings : MonoBehaviour {

#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX

    public static string Player1_Jump_Controller = "_OSX_Player1_Jump_Controller";
    public static string Player2_Jump_Controller = "_OSX_Player2_Jump_Controller";
    public static string Player3_Jump_Controller = "_OSX_Player3_Jump_Controller";
    public static string Player4_Jump_Controller = "_OSX_Player4_Jump_Controller";
    
    public static string Player1_Rotate_Left_Controller = "_OSX_Player1_Rotate_Left_Controller";
    public static string Player2_Rotate_Left_Controller = "_OSX_Player2_Rotate_Left_Controller";
    public static string Player3_Rotate_Left_Controller = "_OSX_Player3_Rotate_Left_Controller";
    public static string Player4_Rotate_Left_Controller = "_OSX_Player4_Rotate_Left_Controller";

    public static string Player1_Rotate_Right_Controller = "_OSX_Player1_Rotate_Right_Controller";
    public static string Player2_Rotate_Right_Controller = "_OSX_Player2_Rotate_Right_Controller";
    public static string Player3_Rotate_Right_Controller = "_OSX_Player3_Rotate_Right_Controller";
    public static string Player4_Rotate_Right_Controller = "_OSX_Player4_Rotate_Right_Controller";

    public static string Submit = "_OSX_Submit";
    public static string Cancel = "_OSX_Cancel";
    public static string Horizontal_Left = "_OSX_Horizontal_Left";
    public static string Horizontal_Right = "_OSX_Horizontal_Right";
    public static string Pause = "_OSX_Pause";

#else

    public static string Player1_Jump_Controller = "Player1_Jump_Controller";
    public static string Player2_Jump_Controller = "Player2_Jump_Controller";
    public static string Player3_Jump_Controller = "Player3_Jump_Controller";
    public static string Player4_Jump_Controller = "Player4_Jump_Controller";
    
    public static string Player1_Rotate_Left_Controller = "Player1_Rotate_Left_Controller";
    public static string Player2_Rotate_Left_Controller = "Player2_Rotate_Left_Controller";
    public static string Player3_Rotate_Left_Controller = "Player3_Rotate_Left_Controller";
    public static string Player4_Rotate_Left_Controller = "Player4_Rotate_Left_Controller";

    public static string Player1_Rotate_Right_Controller = "Player1_Rotate_Right_Controller";
    public static string Player2_Rotate_Right_Controller = "Player2_Rotate_Right_Controller";
    public static string Player3_Rotate_Right_Controller = "Player3_Rotate_Right_Controller";
    public static string Player4_Rotate_Right_Controller = "Player4_Rotate_Right_Controller";

    public static string Submit = "Submit";
    public static string Cancel = "Cancel";
    public static string Horizontal_Left = "Horizontal_Left";
    public static string Horizontal_Right = "Horizontal_Right";
    public static string Pause = "Pause";

#endif

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
