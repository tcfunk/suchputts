﻿using UnityEngine;
using System.Collections;

public class MultiplayerCamera : MonoBehaviour {

    /// <summary>
    /// List of players
    /// @TODO(tyler): This needs to be managed by
    /// some kind of omnipotent game class, rather than
    /// having to drop players in manually.
    /// </summary>
    private Transform[] players;

    /// <summary>
    /// Speed factor for camera position adjustment
    /// </summary>
    public float correctionTime;

    /// <summary>
    /// Minimum Orthographic size for the camera
    /// </summary>
    public float minOrthoSize;

    /// <summary>
    /// Buffer between most extremely-positioned players
    /// </summary>
    public float camBuffer;

    /// <summary>
    /// Tolerance between bounds so that camera is not constantly resizing
    /// </summary>
    public float cameraTolerance;

    /// <summary>
    /// Enables debug statements, including logging and line drawing
    /// </summary>
    public bool DebugMode;

    /// <summary>
    /// Reference to main camera
    /// </summary>
    private Camera cam;

    private Vector3 camVelocity = Vector3.zero;
    private float orthoVelocity = 0;
    private float aspectRatio;

	// Use this for initialization
	void Start () {
        aspectRatio = (float)Screen.width / (float)Screen.height;
	    cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
        //float size = cam.orthographicSize;
        if (players.Length > 0) {
            CenterOnPlayers();
        }
	}

    public void SetPlayers(GameObject[] playerList) {
        players = new Transform[playerList.Length];
        for (int i = 0; i < playerList.Length; i++) {
            players[i] = playerList[i].transform;
        }
    }

    void CenterOnPlayers() {
 
        // Determine player bounds
        float left = 0; float right = 0; float top = 0; float bottom = 0;
        for (int i = 0; i < players.Length; i++) {
            Vector3 player = players[i].position;
            if (i == 0) {
                left = player.x;
                right = player.x;
                top = player.y;
                bottom = player.y;
            } else {
                if (player.x < left) {
                    left  = player.x;
                } else if (player.x > right) {
                    right = player.x;
                }
                if (player.y < bottom) {
                    bottom = player.y;
                } else if (player.y > top) {
                    top = player.y;
                }
            }
        }

        // Move camera toward center of players
        Rect playerBounds = Rect.MinMaxRect(left, bottom, right, top);
        if (this.DebugMode) {
            Debug.DrawLine(playerBounds.min, playerBounds.max, Color.red);
        }

        Vector3 camPosition = Vector3.SmoothDamp(cam.transform.position, new Vector3(playerBounds.center.x, playerBounds.center.y, -1), ref camVelocity, correctionTime*2);
        cam.transform.position = camPosition;


        // Determine required zoom level of camera to contain all players
        Rect camBounds = DetermineCameraBounds();
        if (this.DebugMode) {
            Debug.DrawLine(camBounds.min, camBounds.max, Color.blue);
        }

        // Camera is at least big enough
        if (camBounds.Contains(playerBounds.min) && camBounds.Contains(playerBounds.max)) {
            // Try to shrink a bit if cameraBounds is far enough outside playerBounds
            if (camBounds.min.y < playerBounds.min.y - camBuffer &&
                camBounds.max.y > playerBounds.max.y + camBuffer &&
                camBounds.min.x < (playerBounds.min.x - (aspectRatio*camBuffer)) &&
                camBounds.max.x > (playerBounds.min.x + (aspectRatio*camBuffer))) {
                cam.orthographicSize = Mathf.SmoothDamp(cam.orthographicSize, minOrthoSize, ref orthoVelocity, correctionTime*10);
            }
        } else {
            cam.orthographicSize = Mathf.SmoothDamp(cam.orthographicSize, cam.orthographicSize + 0.5f, ref orthoVelocity, correctionTime);
        }

    }

    Rect DetermineCameraBounds() {
        float halfWidth = aspectRatio*(cam.orthographicSize-camBuffer);
        float halfHeight = cam.orthographicSize-camBuffer;
        float camX = cam.transform.position.x;
        float camY = cam.transform.position.y;
        return new Rect(camX-halfWidth, camY-halfHeight, halfWidth*2, halfHeight*2);
    }
}
