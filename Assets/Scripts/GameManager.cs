﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
#endif
using System.Collections;

public class GameManager : MonoBehaviour {

    /// <summary>
    /// Starting position for each of the players. Also affects their
    /// reset position should they fall out of bounds.
    /// </summary>
    public Transform[] startPositions;

    /// <summary>
    /// Charegbar UI objects for each of the players
    /// </summary>
    public GameObject[] chargeBars;

    public Color[] playerTints;

    /// <summary>
    /// The prefab to be used to spawn players
    /// </summary>
    public GameObject playerPrefab;

    /// <summary>
	/// Reset position (identical to initial position for the time being).
	/// </summary>
    public GameObject OutOfBounds;

    /// <summary>
	/// Goal, or as its known in golf, "le hole"
	/// </summary>
    public GameObject Goal;

    public GameObject Wintext;

    public GameObject MainCamera;

    public GameObject PauseMenu;
    public Button Quit;
    public Button Continue;

    /// <summary>
    /// Keeps track of each of the players for spawning / resetting purposes
    /// Game Manager also sets the attributes of each player so that they do
    /// not have to be manually set (e.g. OOB, Goal, etc.).
    /// </summary>
    private GameObject[] players;

	// Use this for initialization
	void Start () {
        int lesser = ApplicationModel.numPlayers;
        if (ApplicationModel.numPlayers > startPositions.Length) {
            lesser = startPositions.Length;
            Debug.LogWarning("Number of players is greater than number of provided start positions!!");
        }

	    // Create players
        players = new GameObject[lesser];
        for (int i = 0; i < lesser; i++) {
            // Instantiate player object
            var p = (GameObject) Instantiate(playerPrefab, startPositions[i].position, Quaternion.identity);

            // Set player attributes
            p.SendMessage("SetOutOfBounds", this.OutOfBounds);
            p.SendMessage("SetGoal", this.Goal);
            p.SendMessage("SetChargeBar", chargeBars[i]);
            p.SendMessage("SetWintext", this.Wintext);
            p.SendMessage("SetPlayerNumber", i+1);
            p.SendMessage("SetPlayerColor", playerTints[i]);

            players[i] = p;
        }

        MainCamera.SendMessage("SetPlayers", players);

        // Disable pause menu
        UnPause();
        
        // Gather quit and continue buttons
        Continue = PauseMenu.transform.FindChild("Continue").GetComponent<Button>();
        Quit = PauseMenu.transform.FindChild("Quit").GetComponent<Button>();
        
        // Change some controls to OSX version if necessary
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        GameObject es = (GameObject) GameObject.Find("EventSystem");
        StandaloneInputModule inmod = (StandaloneInputModule) es.GetComponent<StandaloneInputModule>();
        inmod.horizontalAxis = "_OSX_Horizontal";
        inmod.verticalAxis = "_OSX_Vertical";
        inmod.submitButton = "_OSX_Submit";
        inmod.cancelButton = "_OSX_Cancel";
#endif
	}
	
	// Update is called once per frame
	void Update () {
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        if (Input.GetButtonUp("_OSX_Pause")) {
#else
        if (Input.GetButtonUp("Pause")) {
#endif
            if (ApplicationModel.IsPaused) {
                UnPause();
            } else {
                Pause();
            }
        }

        if (Input.GetAxis("Horizontal") != 0) {
            Debug.Log(Input.GetAxis("Horizontal"));
        }
	}

    public void Pause() {
        ApplicationModel.IsPaused = true;
        Time.timeScale = 0.01f;
        PauseMenu.SetActive(true);
        EventSystem.current.SetSelectedGameObject(Continue.gameObject);
    }

    public void UnPause() {
        ApplicationModel.IsPaused = false;
        Time.timeScale = 1;
        PauseMenu.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
    }
}
