﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Scenes : MonoBehaviour {

    public Slider PlayerCountSlider;

    public void LoadScene(string sceneName) {
        Application.LoadLevel(sceneName);
    }

    public void SetPlayerCount() {
        ApplicationModel.numPlayers = (int) PlayerCountSlider.value;
    }

}
