﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour {

    ///<summary>
    /// Text displaying the win condition
    ///</summary>
    public GameObject Wintext;

	/// <summary>
	/// Reset position (identical to initial position for the time being).
	/// </summary>
    public Vector2 ResetPosition;
	
	/// <summary>
	/// Number of seconds required to reach maximum power.
	/// </summary>
	public float secondsToCharge;
	
	/// <summary>
	/// Maximum force with which player can strike ball.
	/// </summary>
	public float maxHitForce;
	
	/// <summary>
	/// Maximum force with which player can strike ball.
	/// </summary>
	public float minHitForce;
	
	/// <summary>
	/// Reference to the aiming reticle thingy.
	/// </summary>
	public Transform aimer;
	
	/// <summary>
	/// Speed at which the reticle can rotate.
	/// @NOTE(tyler): This may be become obsolete with controllers,
	/// depending on implementation.
	/// </summary>
	public float rotationSpeed;

    public Vector2 pos;
    public Vector2 size;

    /// <summary>
    /// Reference to Charge Bar UI Object, and private
    /// reference to its Image component (for resizing).
    /// </summary>
    public GameObject chargeBarObject;
    private Image chargeBar;
	
	private float rotAngle = 0;
	private Vector3 rotAxis = Vector3.forward;

    // These should all be assigned by and to the GameManager object
    private GameObject OutOfBounds;
    private GameObject Goal;
	
	/// <summary>
	/// Cached reference to ridgidbody component.
	/// </summary>
	private Rigidbody2D body;

	/// <summary>
	/// Cached reference to CircleCollider component.
	/// </summary>
    private CircleCollider2D _collider;
	
	/// <summary>
	/// The amount of power the player has charged up for their shot,
	/// starting at minHitForce, up to maxHitForce.
	/// @TODO(tyler): Give a visual display of this charging.
	/// </summary>
	private float power;
	
	/// <summary>
	/// The time charged.
	/// </summary>
	private float timeCharged;

	/// <summary>
	/// Used to determine a few things like:
    ///     - Win conditions
    ///     - Input listeners
	/// </summary>
    private int playerNumber;

    private SpriteRenderer ballSprite;

    private AudioSource shootSound;

    private string rotAxisKB;
    private string rotAxisController;
    private string rotLeftController;
    private string rotRightController;

    private string jumpKB;
    private string jumpController;

    void Awake() {
        ballSprite = this.transform.FindChild("GolfBallSprite").GetComponent<SpriteRenderer>();
    }
	
	// Use this for initialization
	void Start () {
        shootSound = GetComponent<AudioSource>();
        this.ResetPosition = new Vector2(this.transform.position.x, this.transform.position.y);
		body = GetComponent<Rigidbody2D>();
        _collider = GetComponent<CircleCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {

        // If Win() screen is active, wait for Esc key
        if (Wintext.activeInHierarchy) {
            this.aimer.gameObject.SetActive(false);
            if (Input.GetKey(KeyCode.Escape)) {
                Wintext.SetActive(false);
                Reset();
            }

        // Is the game paused?
        } else if (IsPaused()) {
            this.aimer.gameObject.SetActive(false);

        // Otherwise process input as usual
        } else {
            this.aimer.gameObject.SetActive(true);
            ProcessRotationalInput(); // Does this need to be in fixedupdate?
            if (IsGrounded()) {
                ProcessShotInput();
            }
        }
	}

    void OnGUI() {
        // Fill the charge bar according to time charged
        chargeBar.fillAmount = power/maxHitForce;

        // Have the charge bar follow the player
        Camera cam = Camera.main;
        Vector3 p = cam.WorldToScreenPoint(transform.position);
        p.y -= 15;
        chargeBarObject.transform.position = p;
    }

    void OnTriggerEnter2D(Collider2D other) {
        // Player wins!
        if (other.gameObject == Goal) {
            Win();
        }
    }

    void OnCollisionEnter2D(Collision2D collision) {
        // Player has fallen off the course, reset their position
        if (collision.collider.gameObject == OutOfBounds) {
            Reset();
        }
    }

    public void SetOutOfBounds(GameObject oob) {
        this.OutOfBounds = oob;
    }

    public void SetGoal(GameObject goal) {
        this.Goal = goal;
    }

    public void SetChargeBar(GameObject cbo) {
        this.chargeBarObject = cbo;
        this.chargeBar = (Image) cbo.GetComponent<Image>();
    }

    public void SetWintext(GameObject text) {
        this.Wintext = text;
        this.Wintext.SetActive(false);
    }

    public void SetPlayerNumber(int n) {
        this.playerNumber = n;
        string prefix = "Player";
        this.name = prefix+n;
        if (n == 1) {
            this.rotAxisKB = prefix+playerNumber+"_Rotate_KB";
            this.jumpKB = prefix+playerNumber+"_Jump_KB";
        }
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        prefix = "_OSX_" + prefix;
        this.rotLeftController = prefix+playerNumber+"_Rotate_Left_Controller";
        this.rotRightController = prefix+playerNumber+"_Rotate_Right_Controller";
        this.jumpController = prefix+playerNumber+"_Jump_Controller";
#endif
        this.rotAxisController = prefix+playerNumber+"_Rotate_Controller";
        this.jumpController = prefix+playerNumber+"_Jump_Controller";
    }

    public void SetPlayerColor(Color c) {
        this.ballSprite.color = c;
    }

    public void Reset() {
        this.transform.position = ResetPosition;
        body.velocity = Vector2.zero;
        body.inertia = 0;
    }

    public void Win() {
        Wintext.SetActive(true);
    }

    public bool IsGrounded() {
        float buffer = _collider.radius + 0.1f;
        Vector2 from = (Vector2)transform.position - new Vector2(0, buffer);
        RaycastHit2D hit = Physics2D.Raycast(from, Vector2.down, 0.1f);
        return hit && hit.transform.gameObject.name != this.name &&
            (hit.transform.tag == "Ground" || hit.transform.tag == "Player");
    }

    /****************************************************************/
	
	private float getAngle(float angle, float step) {
		return (angle + step) % 360;
	}
	
	private bool JumpButtonDown() {
        return  (Input.GetButtonDown(jumpController) ||
                (playerNumber == 1 && Input.GetButtonDown(jumpKB)));
	}
	
	private bool JumpButton() {
        return  (Input.GetButton(jumpController) ||
                (playerNumber == 1 && Input.GetButton(jumpKB)));
	}
	
	private bool JumpButtonUp() {
        return  (Input.GetButtonUp(jumpController) ||
                (playerNumber == 1 && Input.GetButtonUp(jumpKB)));
	}

    private bool LeftRotation() {
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        return Input.GetAxisRaw(rotLeftController) == 1 ||
                (playerNumber == 1 && Input.GetAxisRaw(rotAxisKB) == 1);
#else
        return  Input.GetAxisRaw(rotAxisController) == -1 ||
                (playerNumber == 1 && Input.GetAxisRaw(rotAxisKB) == 1);
#endif
    }

    private bool RightRotation() {
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
        return Input.GetAxisRaw(rotRightController) == 1 ||
                (playerNumber == 1 && Input.GetAxisRaw(rotAxisKB) == -1);
#else
        return  Input.GetAxisRaw(rotAxisController) == 1 ||
                (playerNumber == 1 && Input.GetAxisRaw(rotAxisKB) == -1);
#endif
    }

    private void ProcessRotationalInput() {
        float newAngle = rotAngle;

        if (LeftRotation()) {
            newAngle = getAngle (rotAngle, -rotationSpeed);
        } else if (RightRotation()) {
            newAngle = getAngle (rotAngle, rotationSpeed);
        }

        Quaternion newRotation = Quaternion.AngleAxis(newAngle, rotAxis);
        aimer.rotation = newRotation;
        rotAngle = newAngle;
    }

    private void ProcessShotInput() {
        // Start charging shot when space key is pressed
        if (JumpButtonDown()) {
            timeCharged = 0;
            power = minHitForce;
        }


        // Increase power
        else if (JumpButton()) {
            timeCharged += Time.deltaTime;
            power = Mathf.Lerp (minHitForce, maxHitForce, timeCharged/secondsToCharge);
        }

        // Apply force to ball and reset hit variables
        else if (JumpButtonUp()) {
            Shoot();
        }
    }

    private void Shoot() {
        shootSound.Play();
        float theta = Mathf.Deg2Rad * aimer.rotation.eulerAngles.z;
        Vector2 force = new Vector2(-Mathf.Sin(theta), Mathf.Cos(theta));
        body.AddForce(force*power, ForceMode2D.Impulse);
        power = 0;
        timeCharged = 0;
    }

    private bool IsPaused() {
        return ApplicationModel.IsPaused;
    }

}
